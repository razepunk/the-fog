import tkinter as tk
import requests
import random
import base58
import os
import sys
import pygame
import time

from tkinter import scrolledtext as st
from tkinter import Toplevel, Label
from tkinter import ttk

# Initialize global variables
submit_address_fogs_button = None  # Initialize to None or define with initial value
address_label = None
address_entry = None
submit_address_nifty_button = None  # Initialize to None or define with initial value
# Create the Tkinter window
window = tk.Tk()
window.title("The Fog")
window.configure(bg="#566f59")
label = ttk.Label(window, text="Welcome to The Fog!", font=("Arial", 12))
label.pack(pady="2")

# Create the text area
text_area = st.ScrolledText(window, bg="darkgreen", height=15, width=50, foreground="white",
                            font=("Times New Roman", 10), wrap=tk.WORD)
text_area.pack(expand=True, fill=tk.BOTH)

last_display_index = "1.0"

# Create the input field
input_field = tk.Entry(window)
input_field.after(1, input_field.focus_set())
input_field.pack()
action_buttons = []

# Bind the Enter key to the handle_input function
input_field.bind("<Return>", lambda event: handle_input())


def request_reward(player_address):
    url = "https://singular-picked-phoenix.ngrok-free.app"
    headers = {
        "Content-Type": "application/json",
        "X-API-Key": "4dfabbb4e3bf5ccc9e4e41a97098d7f6007ace5a07ef5660f92ae98c941aed1d"
    }
    data = {
        "player_address": player_address,
        "asset_name": "FOGS"
    }

    try:
        response = requests.post(url, headers=headers, json=data)
        response.raise_for_status()  # This will raise an error if the response code is 4xx or 5xx
        return response.json()
    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP Error: {http_err}")
        print(f"Response content: {response.text}")  # Print the full error content
        return f"HTTP Error: {http_err}"
    except Exception as err:
        return f"Error: {err}"


def request_rewardn(player_address):
    url = "https://singular-picked-phoenix.ngrok-free.app"
    headers = {
        "Content-Type": "application/json",
        "X-API-Key": "4dfabbb4e3bf5ccc9e4e41a97098d7f6007ace5a07ef5660f92ae98c941aed1d"
    }
    data = {
        "player_address": player_address,
        "asset_name": "FOGS/NIFTYMON"
    }

    try:
        response = requests.post(url, headers=headers, json=data)
        response.raise_for_status()  # This will raise an error if the response code is 4xx or 5xx
        return response.json()
    except requests.exceptions.HTTPError as http_err:
        print(f"HTTP Error: {http_err}")
        print(f"Response content: {response.text}")  # Print the full error content
        return f"HTTP Error: {http_err}"
    except Exception as err:
        return f"Error: {err}"


def hide_action_buttons():
    for button in action_buttons:
        button.pack_forget()  # Hide all action buttons


def show_action_buttons():
    for button in action_buttons:
        button.config(font=("Arial", 16), fg="#ffffff", bg="#2f4f7f", borderwidth=2, relief="ridge")
        button.pack(side=tk.LEFT, padx=5)  # Show all action buttons


def initialize_pygame():
    global pygame
    pygame.mixer.init()


def cleanup_pygame():
    global pygame
    pygame.mixer.quit()


initialize_pygame()


def download_sound(url, local_filename):
    print(f"Downloading sound from: {url}")
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(local_filename, 'wb') as f:
            for chunk in response.iter_content(chunk_size=8192):
                f.write(chunk)
        print(f"Sound file saved to {local_filename}")
        return True
    else:
        print(f"Failed to download sound. Status code: {response.status_code}")
        return False


def play_location_sound():
    global current_location
    print("Playing sound for location:", current_location)  # Debug print
    sound_url = graph["locations"].get(current_location, {}).get("sound", None)
    if sound_url:
        print("Sound URL:", sound_url)  # Debug print
        sound_file = "temp_sound.mp3"
        if download_sound(sound_url, sound_file):
            pygame.mixer.init()
            if os.path.exists(sound_file):
                try:
                    pygame.mixer.music.load(sound_file)
                    pygame.mixer.music.play()
                    print("Sound playing")  # Debug print
                except pygame.error as e:
                    text_area.insert(tk.END, f"\nUnable to play sound: {e}")
            else:
                text_area.insert(tk.END, "\nSound file not found.")
        else:
            text_area.insert(tk.END, "\nFailed to download sound file.")
    else:
        text_area.insert(tk.END, "\nNo sound available for this location.")


def listen():
    try:
        play_location_sound()
    except Exception as e:
        text_area.insert(tk.END, f"\nError playing sound: {e}")


def setup_address_ui_fogs():
    global submit_address_fogs_button, address_label, address_entry, address_entry_var

    # Set up UI elements
    address_label = tk.Label(window, text="Enter your address:")
    address_label.pack()
    address_entry_var = tk.StringVar()
    address_entry_var.set("RVN Address")  # This will update the entry field
    address_entry = tk.Entry(window, textvariable=address_entry_var)
    address_entry.pack()

    submit_address_fogs_button = tk.Button(window, text="Submit Address", command=submit_address_fogs)
    submit_address_fogs_button.pack()

    # Pack input_field after setting up address UI
    input_field.pack()


def setup_address_ui_nifty():
    global submit_address_nifty_button, address_label, address_entry, address_entry_var

    # Set up UI elements
    address_label = tk.Label(window, text="Enter your address:")
    address_label.pack()
    address_entry_var = tk.StringVar()
    address_entry_var.set("RVN Address")  # This will update the entry field
    address_entry = tk.Entry(window, textvariable=address_entry_var)
    address_entry.pack()
    submit_address_nifty_button = tk.Button(window, text="Submit Address", command=submit_address_nifty)
    submit_address_nifty_button.pack()

    # Pack input_field after setting up address UI
    input_field.pack()


def submit_address_fogs():
    global submit_address_fogs_button, address_label, address_entry
    player_address = address_entry.get()
    result = request_reward(player_address)
    text_area.insert(tk.END, f"\nReward Result: {result}")
    address_label.pack_forget()
    address_entry.pack_forget()
    submit_address_fogs_button.pack_forget()
    input_field.pack()


def submit_address_nifty():
    global submit_address_nifty_button, address_label, address_entry
    player_address = address_entry.get()
    result = request_rewardn(player_address)
    text_area.insert(tk.END, f"\nReward Result: {result}")
    address_label.pack_forget()
    address_entry.pack_forget()
    submit_address_nifty_button.pack_forget()
    input_field.pack()


def fetch_sage_response1():
    try:
        # Replace with the actual URL of the webpage containing the sage's responses
        response = requests.get("https://gitlab.com/razepunk/the-fog/-/raw/main/assets/response.txt")

        if response.status_code == 200:
            # Split the content by lines
            responses = response.text.splitlines()

            if responses:
                # Return a random response
                return random.choice(responses)
            else:
                return "The sage is silent."
        else:
            return "The sage is silent."
    except requests.RequestException as e:
        return "H"


def fetch_sage_response2():
    try:
        # Replace with the actual URL of the webpage containing the sage's responses
        response = requests.get("https://gitlab.com/razepunk/the-fog/-/raw/main/assets/response2.txt")

        if response.status_code == 200:
            # Split the content by lines
            responses = response.text.splitlines()

            if responses:
                # Return a random response
                return random.choice(responses)
            else:
                return "The sage is silent."
        else:
            return "The sage is silent."
    except requests.RequestException as e:
        return "H"


def press(key):
    global current_text
    if key == "Space":
        key = " "
    elif key == "Back":
        current_text = input_field.get()
        input_field.delete(0, tk.END)
        input_field.insert(0, current_text[:-1])
        return
    elif key == "Enter":
        handle_input()
        return
    current_text = key
    input_field.insert(tk.END, current_text)


# Function to handle user input
def handle_input(event=None):
    global last_display_index
    user_input = input_field.get()
    text_area.insert(tk.END, f"\n> {user_input}")
    last_display_index = text_area.index(tk.END)  # Update the last display index
    input_field.delete(0, tk.END)
    process_input(user_input)

    # Change the color of previously read text after processing the input
    text_area.tag_config("read", foreground="red", background="black")
    text_area.tag_add("read", "1.0", last_display_index)  # Apply the tag to the previously displayed text

    # Ensure the latest action is visible
    text_area.see(tk.END)  # Ensure the latest text is visible


# Function to process user input and update game state
def process_input(user_input):
    global current_location, inventory, weapon_equip, encounter_enemy, enemy_encountered, enemy_defeated, previous_location, keyboard_frame, company
    actions = graph["locations"].get(current_location, {})

    if encounter_enemy:
        keyboard_frame.pack(pady=10)
        hide_action_buttons()  # Hide action buttons when encountering an enemy
        if user_input.lower() == "attack":
            if "Sword" in inventory:
                text_area.insert(tk.END, "\nYou swing your sword and defeat the enemy!")
                encounter_enemy = False
                enemy_encountered = True
                enemy_defeated = True
                keyboard_frame.pack_forget()  # Hide the keyboard frame
                show_action_buttons()  # Show action buttons after the encounter
                last_display_index = text_area.index(tk.END)  # Update the last display index
            else:
                text_area.insert(tk.END, "\nYou don't have a weapon to attack!")
        elif user_input.lower() == "run":
            text_area.insert(tk.END, "\nYou run away from the enemy!")
            enemy_encountered = True
            encounter_enemy = False
            enemy_defeated = False
            if previous_location:
                current_location = previous_location
            keyboard_frame.pack_forget()  # Hide the keyboard frame
            show_action_buttons()  # Show action buttons after the encounter
        elif user_input.lower() == "item":
            text_area.insert(tk.END, "\nYou have the following items:")
            for item in inventory:
                text_area.insert(tk.END, f"\n- {item}")
            if any(item in inventory for item in graph["items"]):
                for item in inventory:
                    if item in graph["items"]:
                        item_data = graph["items"][item]
                        if item_data.get("usable"):
                            if item_data.get("action") == "attack":
                                text_area.insert(tk.END, f"\nYou use the {item} to attack the enemy!")
                                current_location = actions[("Bridge", "cross a rickety bridge")]
                                encounter_enemy = False
                                enemy_encountered = True
                                enemy_defeated = True
                                keyboard_frame.pack_forget()  # Hide the keyboard frame
                                show_action_buttons()  # Show action buttons after the encounter
                        else:
                            text_area.insert(tk.END, "\nInvalid item choice.")
            else:
                text_area.insert(tk.END, "\nYou don't have any usable items.")

        elif user_input.lower() == "send":
            text_area.insert(tk.END, "\nYou have the following Team:")
            for team in company:
                text_area.insert(tk.END, f"\n- {team}")
            if any(team in company for team in graph["teams"]):
                for team in company:
                    if team in graph["teams"]:
                        team_data = graph["teams"][team]
                        if team_data.get("usable"):
                            if team_data.get("action") == "attack":
                                text_area.insert(tk.END, f"\nYou use the {team} to attack the enemy!")
                                current_location = actions[("Bridge", "cross a rickety bridge")]
                                encounter_enemy = False
                                enemy_encountered = True
                                enemy_defeated = True
                                keyboard_frame.pack_forget()  # Hide the keyboard frame
                                show_action_buttons()  # Show action buttons after the encounter
                        else:
                            text_area.insert(tk.END, "\nInvalid team choice.")
            else:
                text_area.insert(tk.END, "\nYou don't have any usable team.")
        else:
            text_area.insert(tk.END, "\nInvalid choice. Please try again.")

    else:
        if user_input.lower() == "quit":
            exit(0)
        if user_input.lower() in [action[0].lower() for action in actions]:
            for action, next_location in actions.items():
                if user_input.lower() == action[0].lower():
                    if next_location == "Swamp" and not encounter_enemy and not enemy_encountered and not enemy_defeated:
                        if "Sword" in inventory:
                            encounter_enemy = True
                    if next_location == "Swamp" and not enemy_defeated:
                        if "Sword" in inventory:
                            encounter_enemy = True
                    if user_input.lower() == "sword" and not weapon_equip:
                        if "Sword" in inventory:
                            text_area.insert(tk.END, "\nwet and empty!")
                        else:
                            text_area.insert(tk.END, "\nYou took the sword of Alexander!")
                            inventory.append("Sword")
                            weapon_equip = True
                    if next_location == "Swamp" and not encounter_enemy and not enemy_encountered and not enemy_defeated:
                        if "Dog" in company:
                            encounter_enemy = True
                    if next_location == "Swamp" and not enemy_defeated:
                        if "Dog" in company:
                            encounter_enemy = True

                    if user_input.lower() == "axe" and not weapon_equip:
                        if "Axe" in inventory:
                            text_area.insert(tk.END, "\nWet and empty!")
                        else:
                            text_area.insert(tk.END, "\nYou took the axe of Øystein!")
                            inventory.append("Axe")
                            weapon_equip = True
                    if current_location == "Fishing_Spot":
                        if user_input.lower() == "fish":
                            if "Trout" in inventory:
                                text_area.insert(tk.END, "\nGrabbing nothing but water")
                            else:
                                text_area.insert(tk.END, "\nYou sit in the Fishing Spot and catch a fish!")
                                inventory.append("Trout")
                    if current_location == "River":
                        while current_location == "River":
                            if user_input.lower() == "row":
                                break
                            if user_input.lower() == "fish":
                                if "Bass" in inventory:
                                    text_area.insert(tk.END, "\nGrabbing nothing but water")
                                    break
                                else:
                                    if random.randrange(0, 9) == 1:
                                        text_area.insert(tk.END, "\nWith a struggle you reel in a huge fish!")
                                        inventory.append("Bass")
                                        break
                                    else:
                                        text_area.insert(tk.END, "\nGrabbing nothing but water")
                                        break
                    if next_location == "Secret_Room":
                        input_field.pack_forget()  # Hide the input field
                        setup_address_ui_fogs()
                    if next_location == "Top":
                        if user_input.lower() == "talk":
                            sage_response = fetch_sage_response1()
                            text_area.insert(tk.END, f"\nThe sage says: '{sage_response}'")
                        if user_input.lower() == "key":
                            if "Key" in inventory:
                                text_area.insert(tk.END, "\nNothing here anymore")
                            else:
                                text_area.insert(tk.END, "\nYou go to the top and find a key!")
                                inventory.append("Key")
                    if current_location == "Flow_State":
                        if user_input.lower() == "talk":
                            sage_response = fetch_sage_response2()
                            text_area.insert(tk.END, f"\nThe sage says: '{sage_response}'")
                    if next_location == "Cave":
                        if "Key" in inventory:
                            text_area.insert(tk.END, "\nYou unlock the Cave and discover a hidden path to the Forest!")
                            inventory.remove("Key")
                            graph["locations"]["Cave"][("Secret Passage", "enter the secret room")] = "Secret_Room"
                            graph["locations"]["Secret_Room"] = {}
                            graph["locations"]["Secret_Room"][("Exit", "exit the secret room")] = "Swamp"
                            graph["locations"]["Exit"] = {}
                        else:
                            text_area.insert(tk.END, "\nThe Cave is locked. You need a key to unlock it.")
                    previous_location = current_location
                    current_location = next_location

                    # Check if entering Under_Water
                    if current_location == "Under_Water" and previous_location != "Under_Water":
                        window.timer = window.after(9000, underwater_death)
                    # Check if leaving Under_Water
                    elif previous_location == "Under_Water" and current_location != "Under_Water":
                        leave_underwater()
                    if current_location == "Mermaid_Cove":
                        setup_address_ui_nifty()
                    if current_location == "Under_Water" and user_input.lower() == "clam":
                        if "Black_Pearl" in inventory:
                            text_area.insert(tk.END, "\nGrabbing nothing but water")
                            window.timer = window.after(1200, underwater_death)
                        else:
                            text_area.insert(tk.END, "\nClam slams shut!")
                            inventory.append("Black_Pearl")

                    if current_location == "Black_Abyss":
                        if user_input.lower() == "yell":
                            if "Dog" in company:
                                text_area.insert(tk.END, "\nThunder claps outside!")
                            else:
                                text_area.insert(tk.END, "\nA large dog emerges")
                                company.append("Dog")
        else:
            text_area.insert(tk.END, "\nInvalid choice. Please try again.")

    # Print the updated game state
    text_area.configure(state='normal')
    print_game_state()
    update_buttons()


# Function to print the game state
def print_game_state():
    if encounter_enemy and not enemy_defeated:
        text_area.insert(tk.END, "\nAn enemy is blocking your way!")
        text_area.insert(tk.END, "\nWhat do you want to do?")
        text_area.insert(tk.END, "\n- Attack")
        text_area.insert(tk.END, "\n- Run")
        text_area.insert(tk.END, "\n- Item")
        if "Dog" in company:
            text_area.insert(tk.END, "\n- Send")
        text_area.see(tk.END)
    if encounter_enemy is False:
        text_area.insert(tk.END, f"\nYou are in the {current_location}")
        text_area.insert(tk.END, "\nAvailable actions:")
        actions = graph["locations"].get(current_location, {})
        for action, next_location in actions.items():
            if 'sound' in action:
                continue
            text_area.insert(tk.END, f"\n* {action[0]} *")
            text_area.insert(tk.END, f"\n- {action[1]} -")
        text_area.insert(tk.END, f"\nInventory: {inventory}")
        if "Dog" in company:
            text_area.insert(tk.END, f"\nCompany: {company}")
        text_area.see(tk.END)


# Remove existing buttons
def update_buttons():
    global actions, keyboard_frame, action_buttons

    # Remove existing buttons
    for button in action_buttons:
        button.destroy()
    action_buttons = []

    actions = graph["locations"].get(current_location, {})

    # Hide action buttons if an enemy is present
    if encounter_enemy and not enemy_defeated:
        input_field.pack()
        submit_button.pack()
        input_field.after(1, input_field.focus_set())
        return  # Return early to prevent adding buttons when an enemy is present

    # Add action buttons
    for action, next_location in actions.items():
        action_text = action[0]

        # Skip creating the button if the next_location has a "sound" entry
        if 'sound' in action:
            continue

        button = tk.Button(window, text=action_text, height=5, width=len(action_text),
                           command=lambda action_text=action_text: usebutton(action_text))
        button.pack()
        action_buttons.append(button)

    # Add the "Listen" button if current location has a "sound" entry
    if 'sound' in graph["locations"].get(current_location, {}):
        if not any(button.cget("text") == "Listen" for button in action_buttons):
            listen_button = tk.Button(window, text="Listen", command=listen)
            listen_button.pack()
            action_buttons.append(listen_button)

    # Pack input_field and submit_button
    input_field.pack()
    submit_button.pack()
    input_field.after(1, input_field.focus_set())


# Animate the buttons

def animate():
    for i, button in enumerate(action_buttons):
        if i % 2 == 0:
            button.config(font=("Arial", 12), fg="#ffffff", bg="#2f5f7f", borderwidth=2, relief="ridge")
        else:
            button.config(font=("Arial", 12), fg="#999999", bg="#565f59", borderwidth=2, relief="ridge")
    window.after(1, animate)


animate()


def usebutton(action_text):
    input_field.delete(0, tk.END)  # Clear the input field
    input_field.insert(0, action_text)


def underwater_death():
    global current_location
    if current_location == "Under_Water":
        death_popup = Toplevel(window)
        death_popup.title("Game Over")
        death_message = Label(death_popup, text="You stayed underwater too long and drowned.", font=("Sans-Serif", 14))
        death_message.pack(pady=10)
        window.after(3000, end_game)


def leave_underwater():
    if window.timer:
        window.after_cancel(window.timer)
        window.timer = None
    text_area.insert(tk.END, "\nYou have left the underwater area just in time!")


def end_game():
    exit(0)


# Define the graph with locations and items
graph = {
    "locations": {
        "Swamp": {
            "sound": "https://gitlab.com/razepunk/the-fog/-/raw/main/assets/swamp-sound.mp3",
            ("Water", "trudge through deep water, getting soaked"): "Fishing_Spot",
            ("Bridge", "cross a rickety bridge"): "Forest",
        },
        "Forest": {
            ("Tree", "climb a tall tree"): "Top",
            ("Path", "follow a winding path"): "Cave",
            ("Hole", "drop into the water again"): "Swamp"
        },
        "Top": {
            ("Talk", "meet an old sage"): "Top",  # NPC here
            ("Ladder", "climb down tree"): "Forest",
            ("Key", "a brown key"): "Top"
        },
        "Cave": {
            "sound": "https://gitlab.com/razepunk/the-fog/-/raw/main/assets/cave-sound.mp3",
            ("Tunnel", "crawl through a narrow tunnel"): "Sandy_Beach"
        },
        "Fishing_Spot": {
            ("Fish", "catch a fish"): "Fishing_Spot",
            ("Waterfall", "reach into the waterfall"): "Waterfall",
            ("Shallow", "trudge back"): "Swamp"
        },
        "Waterfall": {
            ("Sword", "Take the sword"): "Fishing_Spot",
            ("Axe", "Take the Axe"): "Fishing_Spot"
        },
        "Sandy_Beach": {
            ("Left Path", "head towards the seashore"): "West_Coast",
            ("Right Path", "exit through rocky terrain"): "Sandy_Path_Departing"
        },
        # Left Path chain
        "West_Coast": {
            ("Wade", "follow the water to deeper depths"): "Deeper_Water"
        },
        "Deeper_Water": {
            ("Wade_Deeper", "water is up to your chest"): "Under_Water"
        },
        "Under_Water": {
            ("Clam", "grab a clam's pearl"): "Under_Water",
            ("Surface", "swim up and gasp for breath"): "Mermaid_Cove"
        },
        "Mermaid_Cove": {
            "sound": "https://gitlab.com/razepunk/the-fog/-/raw/main/assets/sirensong.mp3",
            ("Stream", "cross a small stream"): "Sandy_Beach_Left_Path_5",
            ("Swim", "jump in with the mermaids"): "Sandy_Beach_Left_Path_5"
        },
        "Sandy_Beach_Left_Path_5": {
            ("Hill", "climb a gentle hill"): "Sandy_Beach_Left_Path_6"
        },
        "Sandy_Beach_Left_Path_6": {
            ("Grove", "enter a dense grove of trees"): "Sandy_Beach_Left_Path_7"
        },
        "Sandy_Beach_Left_Path_7": {
            ("River", "reach a wide river"): "Sandy_Beach_Left_Path_8"
        },
        "Sandy_Beach_Left_Path_8": {
            ("Forest", "enter a small forest area"): "Sandy_Beach_Left_Path_9"
        },
        "Sandy_Beach_Left_Path_9": {
            ("Bridge", "walk across a wooden bridge"): "Sandy_Beach_Left_Path_10"
        },
        "Sandy_Beach_Left_Path_10": {
            ("Meadow", "arrive at a sunny meadow"): "Sandy_Beach_Left_Path_11"
        },
        "Sandy_Beach_Left_Path_11": {
            ("Pond", "find a peaceful pond"): "Sandy_Beach_Left_Path_12"
        },
        "Sandy_Beach_Left_Path_12": {
            ("Trailhead", "arrive at the trailhead"): "Sandy_Beach_Left_Path_13"
        },
        "Sandy_Beach_Left_Path_13": {
            ("Summit", "reach the summit of a small hill"): "Sandy_Beach_Left_Path_14"
        },
        "Sandy_Beach_Left_Path_14": {
            ("Clearing", "find a small clearing"): "Sandy_Beach_Left_Path_15"
        },
        "Sandy_Beach_Left_Path_15": {
            ("Chill Zone", "enter the Chill Zone"): "Icy_Pitt"
        },
        # Right Path chain
        "Sandy_Path_Departing": {
            ("Path", "follow a rocky path"): "Rocky_Path"
        },
        "Rocky_Path": {
            ("Cliff", "navigate along a cliffside"): "Mountain_Path",
            ("Plateau", "a large flat area"): "Small_Pond"
        },
        "Small_Pond": {
            ("Cave", "enter a small cave"): "Cavern_Opening"
        },
        "Mountain_Path": {
            ("Ridge", "walk along a rocky ridge"): "Edge"
        },
        "Cavern_Opening": {
            ("Dark_Tunnel", "feel along the tunnel deeper"): "Black_Abyss"
        },
        "Black_Abyss": {
            ("Yell", "scream into the darkness"): "Black_Abyss",
            ("Run", "escape darkness"): "Swift_Escape",
            ("Hole", "drop into the water again"): "Swamp"
        },
        "Swift_Escape": {
            ("Valley", "descend into a valley"): "Chasm"
        },
        "Chasm": {
            ("Under_Ground", "breathing the dust"): "Central_Basin"
        },
        "Edge": {
            ("Balance", "think and be free"): "Flow_State"
        },
        "Flow_State": {
            ("Talk", "meet an old sage"): "Flow_State",
            ("Drift_Back", "return to previous state"): "Small_Pond"
        },
        "Central_Basin": {
            ("Boat", "travel by a wooden vessel"): "River"
        },
        "River": {
            ("Row", "pull the oars towards you"): "Open_Water",
            ("Fish", "try to catch a fish"): "River"
        },
        "Open_Water": {
            ("Row", "travel deeper into the basin"): "Sandy_Beach_Right_Path_15"
        },
        "Sandy_Beach_Right_Path_15": {
            ("Fire Ring", "arrive at the Fire Ring"): "Fire_Ring"
        },
        # Existing locations
        "Icy_Pitt": {
            ("Relax", "sit by the fire and rest"): "Icy_Pitt",
            ("Inspect", "look around the Chill Zone"): "Icy_Pitt",
            ("Exit", "leave the Chill Zone"): "Sandy_Beach"
        },
        "Fire_Ring": {
            ("Inspect", "look closely at the fire ring"): "Fire_Ring",
            ("Return", "return to the Sandy Beach"): "Sandy_Beach"
        }
    },
    "items": {
        "Trout": {
            "description": "Freshly caught fish",
            "usable": False,
            "action": "cook"
        },
        "Bass": {
            "description": "Large caught fish",
            "usable": False,
            "action": "cook"
        },
        "Sword": {
            "description": "Shiny sword",
            "usable": True,
            "action": "attack",
            "target": "Dragon"
        },
        "Axe": {
            "description": "Wide axe",
            "usable": True,
            "action": "attack",
            "target": "Dragon"
        },
        "Key": {
            "description": "Rusty key",
            "usable": True,
            "action": "unlock",
            "target": "Cave"
        },
        "Black_Pearl": {
            "description": "Blackest thing ever",
            "usable": True,
            "action": "Power"
        },
        "Fire Ring": {
            "description": "Mystical fire ring that seems to radiate warmth",
            "usable": True,
            "action": "inspect",
            "target": "Fire Ring"
        }
    },
    "teams": {
        "Dog": {
            "description": "Incredibly large",
            "usable": True,
            "action": "attack"
        }
    }
}

# Game state variables
current_location = "Swamp"
previous_location = []
inventory = []
company = []
weapon_equip = False
encounter_enemy = False
enemy_encountered = False  # Flag variable to track if the enemy has been encountered
enemy_defeated = False
submit_button = tk.Button(window, text="Submit", command=handle_input)
submit_button.pack()
actions = graph["locations"].get(current_location, {})
update_buttons()
print_game_state()
text_area.configure(state='disabled')
keyboard_frame = tk.Frame(window)
animation = True

# Define keyboard layout
keys = [
    ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'],
    ['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'],
    ['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Enter'],
    ['Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', 'Back'],
    ['Space'],
]

for row in keys:
    frame = tk.Frame(keyboard_frame)
    frame.pack(side=tk.TOP)
    for k in row:
        buttonkb = tk.Button(frame, text=k, command=lambda k=k: press(k), padx=10, pady=10)
        buttonkb.pack(side=tk.LEFT)

# Start the Tkinter event loop
window.mainloop()
