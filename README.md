# The-Fog

Choose your own text adventure

## Getting started
The Fog - Interactive Text Adventure Game

The Fog is a dark and immersive text-based adventure game built using Python's Tkinter GUI library. It combines text-based exploration with sound effects to create a mysterious and interactive experience.
Features

    Interactive Gameplay: Explore various locations, solve puzzles, and face enemies.
    Real-time Sound Effects: Download and play location-based sound effects.
    Reward System: Players can submit their addresses to claim rewards via external modules.
    Inventory Management: Collect items and use them to progress in the game.
    Enemy Encounters: Attack or use items to defeat enemies and continue your journey.

Requirements

    Python 3
    Tkinter (install per OS - sudo apt install python3-tk)
    Base58 (Ravencoin rewards)
    Pygame (for sound playback)
    Requests library (for handling API requests)